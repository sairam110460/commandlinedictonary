function Synonyms(async, common, config, word) {
    this.async = async;
    this.common = common;
    this.config = config;
    this.word = word;
}

Synonyms.prototype.getRequestData = function (callback) {
    var params = this.common.addDefaultDynamicParams([this.config.synonyms.dynamicParams.word], [this.word],
        this.config.synonyms.defaultParams);
    var url = this.config.synonyms.urlPath.start + this.word + this.config.synonyms.urlPath.end;
    this.common.requestGetData(this.config, params, url, function (err, response) {
        if (err) {
            callback(err, null);
        }
        else {
            if (response.statusCode === 200) {
                callback(null, response.body);
            }
            else {
                callback(response.body, null);
            }
        }
    });
};
Synonyms.prototype.validateData = function (responseData, callback) {
    if (responseData.length === 0) {
        return callback("no Synonyms Found", null);
    }
    var data = this.common.getRequireFieldData(responseData, this.config.synonyms.requireParams,
        this.config.synonyms.requireData);
    if (data.length === 0) {
        return callback("no Synonyms Found", null);
    }
    return callback(null, data);
};
Synonyms.prototype.accessMethods = function (callback) {
    var main = this;
    this.async.waterfall([function (callback) {
        main.getRequestData(function (err, responseData) {
            if (err) {
                callback(err, null);
            }
            else {
                // console.log(responseData);
                callback(null, responseData);
            }
        });
    }, function (responseData, callback) {
        main.validateData(responseData, function (err, responseData) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, responseData);
            }
        });
    },function (responseData, callback) {
        console.log("Word Synonyms");
        main.common.outputDisplayArray(responseData,main.config.synonyms.requireData,function(err,result){
            if(err){
                callback(err,null);
            }
            else{
                callback(null,result);
            }
        });
    }], function (err, responseData) {
        if (err) {
            callback(err, null);
        }
        else {
            callback(null, responseData);
        }
    });
};
exports.synonyms = Synonyms;