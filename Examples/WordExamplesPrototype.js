function Examples(async, common, config, word) {
    this.async = async;
    this.common = common;
    this.config = config;
    this.word = word;
}

Examples.prototype.getRequestData = function (callback) {
    var params = this.common.addDefaultDynamicParams([this.config.examples.dynamicParams.word], [this.word],
        this.config.examples.defaultParams);
    var url = this.config.examples.urlPath.start + this.word + this.config.examples.urlPath.end;
    this.common.requestGetData(this.config, params, url, function (err, responseData) {
        if (err) {
            callback(err, null);
        }
        else {
            if (responseData.statusCode === 200) {
                callback(null, responseData.body);
            }
            else {
                callback(responseData.body, null);
            }
        }
    });
};
Examples.prototype.validateData = function (responseData, callback) {
    if (typeof (responseData[this.config.examples.requireParams.examples]) === "undefined" ||
        responseData[this.config.examples.requireParams.examples].length === 0) {
        // console.log(responseData["examples"]);
        return callback("No Examples Found", null);
    }
    // var examples =this.common.getRequireData(responseData["examples"],)
    // for (var eachExamples = 0; eachExamples < responseData["examples"].length; eachExamples++) {
    //     examples.push(responseData["examples"][eachExamples]["text"]);
    // }
    // return callback(null, examples);
    return callback(null, this.common.getRequireData(responseData[this.config.examples.requireParams.examples],
        this.config.definition.requireData));
};
Examples.prototype.accessMethods = function (callback) {
    var main = this;
    this.async.waterfall([function (callback) {
        main.getRequestData(function (err, responseExamples) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, responseExamples);
            }
        });
    }, function (responseExamplesofWord, callback) {
        main.validateData(responseExamplesofWord, function (err, Examples) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, Examples);
            }
        });
    },function (responseData, callback) {
        console.log("Word Examples");
        main.common.outputDisplay(responseData,main.config.examples.requireData,function(err,result){
            if(err){
                callback(err,null);
            }
            else{
                callback(null,result);
            }
        });
    }], function (err, responseExamples) {
        if (err) {
            callback(err, null);
        }
        else {
            callback(null, responseExamples)
        }
    })
};
exports.examples = Examples;
