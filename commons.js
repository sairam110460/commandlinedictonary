var events = require("events");
var eventEmitter = new events.EventEmitter();
exports.eventEmitter = eventEmitter;
var request = require('request');
exports.requestGetData = function (config, params, requireUrl, callback) {
    request({
        "headers": {"content-type": "application/json", "api_key": config.api_key},
        "url": config.requestUrl + requireUrl,
        "json": true,
        "body": params
    }, function (err, response) {
        if (err) {
            callback(err, null);
        }
        else {
            callback(null, response)
        }
    });
};
exports.addDefaultDynamicParams = function (dynamicParamsArray, dynamicValuesArray, defaultParams) {
    var finalParams = {};
    for (var eachDynamic = 0; eachDynamic < dynamicParamsArray.length; eachDynamic++) {
        finalParams[dynamicParamsArray[eachDynamic]] = dynamicValuesArray[eachDynamic];
    }
    var getDefaultKeys = Object.keys(defaultParams);
    for (var eachDefault = 0; eachDefault < getDefaultKeys.length; eachDefault++) {
        finalParams[getDefaultKeys[eachDefault]] = defaultParams[getDefaultKeys[eachDefault]];
    }
    return finalParams;
};
exports.getRequireData = function (reponseDataArray, requirePrams) {
    var totalData = [];
    for (var eachDocument = 0; eachDocument < reponseDataArray.length; eachDocument++) {
        for (var eachParams = 0; eachParams < requirePrams.length; eachParams++) {
            if (reponseDataArray[eachDocument][requirePrams[eachParams]]) {
                var data = {};
                data[requirePrams[eachParams]] = reponseDataArray[eachDocument][requirePrams[eachParams]];
                totalData.push(data);
            }
        }
    }
    // console.log(totalData);
    return totalData;

};
exports.getRequireFieldData = function (responseArray, checkObject, requirePrams) {
    var totalData = [];
    var requireKeys = Object.keys(checkObject);
    for (var eachDocument = 0; eachDocument < responseArray.length; eachDocument++) {
        for (var eachKey = 0; eachKey < requireKeys.length; eachKey++) {
            if (responseArray[eachDocument][requireKeys[eachKey]] &&
                responseArray[eachDocument][requireKeys[eachKey]] === checkObject[requireKeys[eachKey]]) {
                var data = {};
                for (var eachParam = 0; eachParam < requirePrams.length; eachParam++) {
                    if (responseArray[eachDocument][requirePrams[eachParam]]) {
                        data[requirePrams[eachParam]] = responseArray[eachDocument][requirePrams[eachParam]];
                        totalData.push(data);
                    }
                }
            }
        }
    }
    return totalData;
};
exports.outputDisplay = function (resposneArray, requireParams, callback) {
    // console.log("response",resposneArray,requireParams);
    for (var eachResponse = 0; eachResponse < resposneArray.length; eachResponse++) {
        // console.log(resposneArray)
        for (var eachParam = 0; eachParam < requireParams.length; eachParam++) {
            // console.log(requireParams[eachParam],resposneArray[eachResponse]);
            if (resposneArray[eachResponse][requireParams[eachParam]]) {
                console.log("\t", eachResponse + 1, " ", resposneArray[eachResponse][requireParams[eachParam]]);
            }
        }
    }
    callback(null, resposneArray);
};
exports.outputDisplayArray = function (resposneArray, requireParams, callback) {
    // console.log("response",resposneArray,requireParams);
    for (var eachResponse = 0; eachResponse < resposneArray.length; eachResponse++) {
        // console.log(resposneArray)
        for (var eachParam = 0; eachParam < requireParams.length; eachParam++) {
            // console.log(requireParams[eachParam],resposneArray[eachResponse]);
            if (resposneArray[eachResponse][requireParams[eachParam]]) {
                // console.log("\t", eachResponse + 1, " ",resposneArray[eachResponse][requireParams[eachParam]]);
                for (var each = 0; each < resposneArray[eachResponse][requireParams[eachParam]].length; each++) {
                    console.log("\t", each + 1, " ", resposneArray[eachResponse][requireParams[eachParam]][each]);
                }
            }
        }
    }
    callback(null, resposneArray);
};
function checkGivenDate(date) {
    var regEx = /^\d{4}-\d{2}-\d{2}$/;//check the format digits requiresd
    if (!date.match(regEx)) return false;  //if doesnot match
    var dateConversion = new Date(date);//date conversion
    if (!dateConversion.getTime() && dateConversion.getTime() !== 0) return false; // doesnot have getTime
    return dateConversion.toISOString().slice(0, 10) === date;

}
exports.validateDate = checkGivenDate;
function checkGivenWord(givenWord) {
    var regularExpression = /^[a-zA-Z\s\-]+$/;
    if (!givenWord.match(regularExpression)) return false;
    return true;
}
exports.validateWord = checkGivenWord;

exports.displayMenu = {
    "1": "Word Definition",
    "2": "Word Synonyms",
    "3": "Word Antonyms",
    "4": "Word Examples",
    "5": "Word Full Dict",
    "6": "Word of the Day Full Dict",
    "7": "Word Game"
};
exports.WordGamedisplay = {
    "1": "try another word",
    "2": "defination/synonyms/examples/definition",
    "3": 'dispaly full dict quit'
};

exports.constNames = {
    "WordDefinition": "Word Definition",
    "WordSynonyms": "Word Synonyms",
    "WordAntonyms": "Word Antonyms",
    "WordExamples": "Word Examples",
    "WordFullDict": "Word Full Dict",
    "WordoftheDayFullDict": "Word of the Day Full Dict",
    "WordGame": "Word Game"
};