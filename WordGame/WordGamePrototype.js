function WordGame(async, common, config, word) {
    this.async = async;
    this.common = common;
    this.config = config;
    this.word = word;
}

WordGame.prototype.searchWord = function (callback) {
    var params = this.common.addDefaultDynamicParams([this.config.wordGame.dynamicParams.query], [this.word],
        this.config.wordGame.defaultParams);
    var url = this.config.wordGame.urlPath.start + this.word;
    this.common.requestGetData(this.config, params, url, function (err, response) {
        if (err) {
            callback(err, null);
        }
        else {
            if (response.statusCode === 200) {
                callback(null, response.body);
            }
            else {
                callback(response.body, null);
            }
        }
    });
};
WordGame.prototype.validateData = function (responseData, callback) {
    if (responseData["totalResults"] <= 0) {
        callback("not Data Found", null);
    }
    else {
        callback(null, responseData);
    }
};
WordGame.prototype.accessMethods = function (callback) {
    var main = this;
    this.async.waterfall([function (callback) {
        main.searchWord(function (err, response) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, response);
            }
        });
    }, function (responseData, callback) {
        main.validateData(responseData, function (err, result) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, result);
            }
        });
    }, function (resultFount, callback) {
        main.common.eventEmitter.once("WordSynonymsResult", function (err, result) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, result);
            }
        });
        main.common.eventEmitter.emit(main.common.constNames.WordSynonyms, main.word, main.async, main.common,
            main.config, "WordSynonymsResult", null)
    }], function (err, result) {
        if (err) {
            callback(err, null);
        }
        else {
            callback(null, result);
        }
    });
};
exports.wordGame = WordGame;