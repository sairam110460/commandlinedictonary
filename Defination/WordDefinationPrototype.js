function Defination(async, word, common, config) {
    this.async = async;
    this.word = word;
    this.common = common;
    this.config = config;
}
Defination.prototype.getRequestData = function (callback) {
    var params = this.common.addDefaultDynamicParams([this.config.definition.dynamicParams.word], [this.word],
        this.config.definition.defaultParams);
    var url = this.config.definition.urlPath.start + this.word + this.config.definition.urlPath.end;
    this.common.requestGetData(this.config, params, url, function (err, responseDefination) {
        if (err) {
            callback(err, null);
        }
        else {
            if (responseDefination.statusCode === 200) {
                // console.log(responseDefination.body);
                callback(null, responseDefination.body);
            }
            else {
                // console.log(responseDefination.statusCode,responseDefination.body);
                callback(responseDefination.statusCode, null);
            }
        }
    });
};
Defination.prototype.validateData = function (response, callback) {
    if (!Array.isArray(response) || response.length === 0) {
        return callback("No words Found", null);
    }
    // console.log(response);
    return callback(null, this.common.getRequireData(response, this.config.definition.requireData));
};
Defination.prototype.accessMethods = function (callback) {
    var main = this;
    this.async.waterfall([function (callback) {
        main.getRequestData(function (err, responseData) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, responseData);
            }
        });
    }, function (responseDefinition, callback) {
        main.validateData(responseDefinition, function (err, definionArray) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, definionArray)
            }
        });
    },function (responseData, callback) {
        console.log("Word Defination");
        main.common.outputDisplay(responseData,main.config.definition.requireData,function(err,result){
            if(err){
                callback(err,null);
            }
            else{
                callback(null,result);
            }
        });
    }], function (err, definitions) {
        if (err) {
            callback(err, null);
        }
        else {
            callback(null, definitions);
        }
    })
};
exports.definition = Defination;
