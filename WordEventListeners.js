var common = require('./commons');
var antonymsOfWord = require('./Antonyms/WordAtonymsPrototype');
var definationOfWord = require('./Defination/WordDefinationPrototype');
var examplesOfWord = require('./Examples/WordExamplesPrototype');
var synonymsOfWord = require('./Synonyms/WordSynonymsPrototype');
var wordOftheDay = require('./WordOftheDay/WordofTheDayprototype');
var wordGame = require('./WordGame/WordGamePrototype');
common.eventEmitter.on(common.constNames.WordDefinition, function (word, async, common, config, ackName, readLine) {
    // console.log("emit the events");
    var wordDefination = new definationOfWord.definition(async, word, common, config);
    wordDefination.accessMethods(function (err, result) {
        // console.log("Word Definition");
        if (err) {
            if (typeof err === "string" && err.indexOf("No words Found") > -1) {
                console.log(err);
                common.eventEmitter.emit(ackName, null, []);
            }
            else {
                common.eventEmitter.emit(ackName, err, null);
            }
        }
        else {
            common.eventEmitter.emit(ackName, null, result);
        }
    });
});
common.eventEmitter.on(common.constNames.WordSynonyms, function (word, async, common, config, ackName, readLine) {
    var wordSynonym = new synonymsOfWord.synonyms(async, common, config, word);
    wordSynonym.accessMethods(function (err, result) {
        // console.log("Word Synonyms");
        if (err) {
            if (typeof err === "string" && err.indexOf("no Synonyms Found") > -1) {
                console.log(err);
                common.eventEmitter.emit(ackName, null, []);
            }
            else {
                common.eventEmitter.emit(ackName, err, null);
            }
        }
        else {
            common.eventEmitter.emit(ackName, null, result);
        }
    });
});
common.eventEmitter.on(common.constNames.WordAntonyms, function (word, async, common, config, ackName, readLine) {
    var wordAntonyms = new antonymsOfWord.antonyms(async, common, config, word);
    wordAntonyms.accessMethods(function (err, result) {
        // console.log("Word Antonyms");
        if (err) {
            if (typeof err === "string" && err.indexOf("no Antonyms Found") > -1) {
                console.log(err);
                common.eventEmitter.emit(ackName, null, []);
            }
            else {
                common.eventEmitter.emit(ackName, err, null);
            }
        }
        else {
            common.eventEmitter.emit(ackName, null, result);
        }
    });
});
common.eventEmitter.on(common.constNames.WordExamples, function (word, async, common, config, ackName, readLine) {
    var wordExamples = new examplesOfWord.examples(async, common, config, word);
    wordExamples.accessMethods(function (err, result) {
        // console.log("Word Examples");
        if (err) {
            if (typeof err === "string" && err.indexOf("No Examples Found") > -1) {
                console.log(err);
                common.eventEmitter.emit(ackName, null, []);
            }
            else {
                common.eventEmitter.emit(ackName, err, null);
            }
        }
        else {
            common.eventEmitter.emit(ackName, null, result);
        }
    });
});
common.eventEmitter.on(common.constNames.WordFullDict, function (word, async, common, config, ackName, readLine) {
    async.parallel([function (callback) {
        common.eventEmitter.once("WordFullDictDef", function (err, result) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, result);
            }
        });
        common.eventEmitter.emit(common.constNames.WordDefinition, word, async, common, config, "WordFullDictDef");
    }, function (callback) {
        common.eventEmitter.once("WordFullDictSym", function (err, result) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, result);
            }
        });
        common.eventEmitter.emit(common.constNames.WordSynonyms, word, async, common, config, "WordFullDictSym");

    }, function (callback) {
        common.eventEmitter.once("WordFullDictAnt", function (err, result) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, result);
            }
        });
        common.eventEmitter.emit(common.constNames.WordAntonyms, word, async, common, config, "WordFullDictAnt");
    }, function (callback) {
        common.eventEmitter.once("WordFullDictExa", function (err, result) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, result);
            }
        });
        common.eventEmitter.emit(common.constNames.WordExamples, word, async, common, config, "WordFullDictExa");
    }], function (err, finalResult) {
        if (err) {
            common.eventEmitter.emit(ackName, err, null)
        }
        else {
            common.eventEmitter.emit(ackName, null, finalResult);
        }
    });

});
common.eventEmitter.on(common.constNames.WordoftheDayFullDict, function (date, async, common, config, ackName, readLine) {
    var wordoftheDay = new wordOftheDay.wordOftheDay(async, common, config, date);
    async.waterfall([function (callback) {
        wordoftheDay.accessMethods(function (err, result) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, result);
            }
        });
    }, function (word, callback) {
        common.eventEmitter.once("WordofTheDay", function (err, result) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, result);
            }
        });
        common.eventEmitter.emit(common.constNames.WordFullDict, word, async, common, config, "WordofTheDay");
    }], function (err, result) {
        if (err) {
            common.eventEmitter.emit(ackName, err, null)
        }
        else {
            common.eventEmitter.emit(ackName, null, result);
        }
    });
});
common.eventEmitter.on(common.constNames.WordGame, function (word, async, common, config, ackName, readLine) {
    async.waterfall([function (callback) {
        readLine.question("Enter the Word :", function (word) {
            callback(null, word);
        })
    }, function (word, callback) {
        var wordGameProto = new wordGame.wordGame(async, common, config, word);
        wordGameProto.accessMethods(function (err, result) {
            if (err) {
                if (typeof err === "string" && err.indexOf("not Data Found") > -1) {
                    console.log("Word Not Found");
                    // process.exit(1);
                    callback(null, false, word);
                }
                else {
                    callback(err, null, word);
                }
            }
            else {
                callback(null, true, word);
            }
        });
    }, function (displayMenu, word, callback) {
        if (!displayMenu) {
            var keysWordGame = Object.keys(common.WordGamedisplay);
            for (var eachopt = 0; eachopt < keysWordGame.length; eachopt++) {
                console.log("\t", keysWordGame[eachopt], " ", common.WordGamedisplay[keysWordGame[eachopt]]);
            }
            callback(null, displayMenu, word);
        }
        else {
            callback(null, displayMenu, word);
        }
    }, function (displayMenu, word, callback) {
        if (!displayMenu) {
            readLine.question("Enter the Option :", function (option) {
                callback(null, parseInt(option), word);
            });
        }
        else {
            callback(null, 1, word);
        }
    }, function (option, word, callback) {
        if (common.WordGamedisplay[option]) {
            callback(null, option, word);
        }
        else {
            callback("invalid option Send", null);
        }
    }, function (option, word, callback) {
        common.eventEmitter.once(option, function (err, result) {
            if (option === 3) {
                if (err) {
                    callback(err, null);
                }
                else {
                    callback(null, result);
                }
            }
            else {
                common.eventEmitter.once("WordGameAgain", function (err, result) {
                    if (err) {
                        common.eventEmitter.emit(ackName, err, null);
                    }
                    else {
                        common.eventEmitter.emit(ackName, null, result);
                    }
                });
                common.eventEmitter.emit(common.constNames.WordGame, word, async, common, config, "WordGameAgain", readLine);
            }
        });
        if (option === 1) {
            common.eventEmitter.emit(option, null, "done");
        }
        else {
            common.eventEmitter.emit(common.constNames.WordFullDict, word, async, common, config, option, readLine);
        }
    }], function (err, result) {
        if (err) {
            common.eventEmitter.emit(ackName, err, null)
        }
        else {
            common.eventEmitter.emit(ackName, null, result);
        }
    });
});
