var async = require('async');
var readline = require('readline');
var rl = readline.createInterface({"input": process.stdin, "output": process.stdout});
var common = require('./commons');
var config = require('./config/config.json');
var WordListeners = require('./WordEventListeners');


function Users(readline, common, config) {
    this.readline = readline;
    this.common = common;
    this.config = config;
}
Users.prototype.getInputFromUser = function (question, callback) {
    this.readline.question(question, function (userInput) {
        return callback(null, userInput);
    });
};
Users.prototype.checkWord = function (userEnterWord, callback) {
    if (this.common.validateWord(userEnterWord)) {
        callback(null, userEnterWord);
    }
    else {
        callback("Please Enter Valid Word", null);
    }
};
Users.prototype.checkDate = function (userEnterDate, callback) {
    if (this.common.validateDate(userEnterDate)) {
        callback(null, userEnterDate);
    }
    else {
        callback("Invalid Date Enter", null);
    }
};
Users.prototype.displayMenu = function () {
    var options = Object.keys(this.common.displayMenu);
    for (var eachMenu = 0; eachMenu < options.length; eachMenu++) {
        console.log(options[eachMenu] + ")  " + this.common.displayMenu[options[eachMenu]]);
    }
    return true;
};
Users.prototype.chooseOption = function (callback) {
    this.getInputFromUser("Enter Option: ", function (err, option) {
        if (err) {
            callback(err, null);
        }
        else {
            callback(null, option)
        }
    });
};
Users.prototype.checkOptionGetResult = function (option, word, callback) {
    this.common.eventEmitter.once("atChecking", function (err, result) {
        if (err) {
            callback(err, null);
        }
        else {
            callback(null, result);
        }
    });
    this.common.eventEmitter.emit(this.common.displayMenu[option], word, async, common, config, "atChecking", rl);
};


function start() {
    var user = new Users(rl, common, config);
    async.waterfall([function (callback) {
        if (user.displayMenu()) {
            user.chooseOption(function (err, option) {
                if (err) {
                    callback(err, null);
                }
                else {
                    callback(null, option);
                }
            });
        }
        else {
            callback("Error While displayMenu", null);
        }
    }, function (option, callback) {
        if (!common.displayMenu[option]) {
            callback("Enter Invalid Option", null);
        }
        else {
            callback(null, option);
        }
    }, function (option, callback) {
        if (common.displayMenu[option] === common.constNames.WordoftheDayFullDict) {
            user.getInputFromUser("Enter the Valid Date :(YYYY-MM-DD)", function (err, date) {
                if (err) {
                    callback(err, null);
                }
                else {
                    if (common.validateDate(date)) {
                        callback(null, option, date);
                    }
                    else {
                        callback("Enter Valid Date", null);
                    }
                }
            });
        }
        else if(common.displayMenu[option]===common.constNames.WordGame){
            callback(null,option,null);
        }
        else {
            user.getInputFromUser("Enter the Word to Find in Dictonary: ", function (err, result) {
                if (err) {
                    callback(err, null);
                }
                else {
                    // console.log(result);
                    if (common.validateWord(result)) {
                        callback(null, option, result);
                    }
                    else {
                        callback("invalid Word Enter", null);
                    }
                }
            });
        }
    }, function (option, word, callback) {
        user.checkOptionGetResult(option, word, function (err, result) {
            if (err) {
                // console.log(err);
                callback(err, null);
            }
            else {
                callback(null, result);
            }
        });
    }], function (err, completed) {
        if (err) {
            console.log(err);
        }
        console.log();
        // console.log("error", err, "result", completed);
        setImmediate(function () {
            start();
        });
    });
}
start();