function Antonyms(async, common, config, word) {
    this.async = async;
    this.common = common;
    this.config = config;
    this.word = word;
}

Antonyms.prototype.getRequestData = function (callback) {
    var params = this.common.addDefaultDynamicParams([this.config.antonyms.dynamicParams.word], [this.word],
        this.config.antonyms.defaultParams);
    var url = this.config.antonyms.urlPath.start + this.word + this.config.antonyms.urlPath.end;
    this.common.requestGetData(this.config, params, url, function (err, response) {
        if (err) {
            callback(err, null);
        }
        else {
            if (response.statusCode === 200) {
                callback(null, response.body);
            }
            else {
                callback(response.body, null);
            }
        }
    });
};
Antonyms.prototype.validateData = function (responseData, callback) {
    if (responseData.length === 0) {
        return callback("no Antonyms Found", null);
    }
    var words = this.common.getRequireFieldData(responseData, this.config.antonyms.requireParams,
        this.config.antonyms.requireData);
    if (words.length === 0) {
        return callback("no Antonyms Found", null);
    }
    return callback(null, words);
};
Antonyms.prototype.accessMethods = function (callback) {
    var main = this;
    this.async.waterfall([function (callback) {
        main.getRequestData(function (err, responseAntonyms) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, responseAntonyms);
            }
        });
    }, function (responseAntonyms, callback) {
        main.validateData(responseAntonyms, function (err, responseData) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, responseData);
            }
        });
    }, function (responseData, callback) {
        console.log("Word Antonyms");
        main.common.outputDisplayArray(responseData,main.config.antonyms.requireData,function(err,result){
            if(err){
                callback(err,null);
            }
            else{
                callback(null,result);
            }
        });
    }], function (err, responseData) {
        if (err) {
            callback(err, null);
        }
        else {
            callback(null, responseData);
        }
    });
};
exports.antonyms = Antonyms;